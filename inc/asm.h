/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 18:55:47 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 18:55:54 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_H
# define ASM_H

# include <stdbool.h>
# include "op.h"
# include "../libft/includes/libft.h"
# include <fcntl.h>

typedef struct				s_label
{
	char					*name;
}							t_label;

typedef struct				s_arg
{
	unsigned				type;
	unsigned				code;
	char					label_flag;
	char					*label_name;
	unsigned				len;
	unsigned				value;
}							t_arg;

typedef struct				s_option
{
	char					*name;
	int						args_num;
	unsigned				args[3];
	bool					is_have_args;
	int						cycles_to_do_op;
	int						dir_size;
}							t_op;

typedef struct				s_instruction
{
	char					*name;
	char					codage;
	int						opcode;
	unsigned				hex_id_for_write;
	t_arg					*arg1;
	t_arg					*arg2;
	t_arg					*arg3;
	t_list					*labels;
}							t_instruction;

typedef struct				s_asm
{
	int						fd;
	int						line_number;
	int						i_of_code;
	char					*file_name_cor;
	t_header				champ_header;
	t_list					*instructions;
	char					*line;
}							t_asm;

void						asm_exit(t_asm *a,
								int is_with_error, char *error_msg);
void						asm_free_memory(t_asm *a);

void						asm_get_name_and_comment(t_asm *a);

void						skip_whitespaces_and_comments(t_asm *a,
		char **line);
void						skip_useless_area(t_asm *a);
int							skip_useless(t_asm *a);
int							strisword(const char *line, int start);
long int					ft_atol(char *string);
unsigned int				digit_count(long long n);
int							ft_isblank(char c);
int							check_label_content(char label_letter);
int							check_label_content2(t_asm *a, int *i);
void						push_tail(t_list **head, void *data, int size);

t_instruction				*get_last_instruction(t_asm *a);

void						write_whole_bytecode(t_asm *a);
void						get_prog_size(t_asm *a, t_list **head);

void						int_to_bytecode(char *data, int pos, int value,
								size_t size);

void						asm_parse_insts(t_asm *a);

int							parse_labels(t_asm *a);
int							check_labels(t_list **inst, int l_index,
		t_list *tmp, t_instruction *i);

int							parse_arguments(t_asm *a,
								t_instruction *inst, int i);

int							check_arg(t_instruction *instruction, int id);
int							parse_arg(t_asm *a,
								t_instruction *inst, char *line, int id);
int							parse_next_arg(t_asm *a,
								t_instruction *inst, int id);

t_asm						*asm_init(void);
t_instruction				*init_instruction(t_asm *asm_instance);
t_label						*label_init(char *label_name);
t_arg						*arg_init(char code, char label_flag,
		int value, char *label_name);

static	const t_op			g_op_tab[16] =
{
	{"live", 1, {T_DIR}, 0, 10, 4},
	{"ld", 2, {T_DIR | T_IND, T_REG}, 1, 5, 4},
	{"st", 2, {T_REG, T_IND | T_REG}, 1, 5, 4},
	{"add", 3, {T_REG, T_REG, T_REG}, 1, 10, 4},
	{"sub", 3, {T_REG, T_REG, T_REG}, 1, 10, 4},
	{"and", 3, {T_REG | T_DIR | T_IND, T_REG | T_IND | T_DIR, T_REG}, 1, 6, 4},
	{"or", 3, {T_REG | T_IND | T_DIR, T_REG | T_IND | T_DIR, T_REG}, 1, 6, 4},
	{"xor", 3, {T_REG | T_IND | T_DIR, T_REG | T_IND | T_DIR, T_REG}, 1, 6, 4},
	{"zjmp", 1, {T_DIR}, 0, 20, 2},
	{"ldi", 3, {T_REG | T_DIR | T_IND, T_DIR | T_REG, T_REG}, 1, 25, 2},
	{"sti", 3, {T_REG, T_REG | T_DIR | T_IND, T_DIR | T_REG}, 1, 25, 2},
	{"fork", 1, {T_DIR}, 0, 800, 2},
	{"lld", 2, {T_DIR | T_IND, T_REG}, 1, 10, 4},
	{"lldi", 3, {T_REG | T_DIR | T_IND, T_DIR | T_REG, T_REG}, 1, 50, 2},
	{"lfork", 1, {T_DIR}, 0, 1000, 2},
	{"aff", 1, {T_REG}, 1, 2, 4}
};

#endif
