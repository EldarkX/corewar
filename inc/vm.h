/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 20:22:02 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/02 20:22:04 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VM_H
# define VM_H

# include <stdbool.h>
# include "op.h"
# include "../libft/includes/libft.h"
# include <fcntl.h>

# define PLAYER_CODE 		(-1)

typedef struct s_env		t_env;
typedef struct s_option		t_op;
typedef struct s_arena		t_arena;
typedef struct s_process	t_process;
typedef struct s_champion	t_champion;
typedef struct s_debug		t_debug;
typedef union u_number		t_number;

struct						s_debug
{
	bool					d1;
	bool					d2;
	bool					d3;
};

struct						s_option
{
	void					(*function)(t_env *env, t_process *process);
	int						args[3];
	bool					is_have_args;
	int						cycles_to_do_op;
	int						dir_size;
};

struct						s_champion
{
	int						id;
	char					*name;
	char					*comment;
	char					*exec_code;
	int						exec_code_size;
};

struct						s_arena
{
	unsigned char			arena[MEM_SIZE];
	t_list					*procs;
};

struct						s_env
{
	short					nbr_checks;
	int						nbr_cycles_to_checkup;
	unsigned				nbr_live;
	t_arena					arena;
	t_debug					debug;
	bool					dump_mode;
	short					num_of_champions;
	long long				nbr_cycles;
	int						cycle_to_die;
	t_champion				*winner;
	t_champion				*champion[MAX_PLAYERS];
};

struct						s_process
{
	int						id;
	int						reg[REG_NUMBER];
	int						pc;
	bool					carry;
	bool					will_live;
	int						cycles_to_do_op;
	int						op_num;
	int						args[3];
	int						codage[3];
};

union						u_number
{
	unsigned char			numbers[4];
	int						number;
};

void						set_debug_and_other(t_env *env,
										char *argv, t_list **names);

bool						checkup(t_env *env);
bool						processes_checkup(t_env *env);

void						kill_champs(t_env *env);
bool						check_for_last_champ(t_env *env);

void						show_usage_and_exit(void);

void						free_champ(t_champion *champ);
void						free_all(t_env *env);
void						exit_with_error(t_env *env, char *error_text);

t_process					*init_process(void);
t_champion					*init_champ(int id);
void						init_arena_and_processes(t_env *env);
void						init_env(t_env *env);

void						debug_show_players_data(t_env *env);
void						hex_dump(unsigned char *arena, int len);
void						set_processes_id(t_env *env);
void						set_args_codage_null(t_process *p);

void						parse_champion(char *file_name, int id, t_env *env);
void						parse_args(int argc, char **argv, t_env *env);
void						check_if_any_champs_present(t_env *env);
void						parse_list_with_names(t_env *env, t_list **head);
void						check_name(t_env *env, char *name);

t_list						*ft_new_elem(void *data, int size);
void						push_tail(t_list **head, void *new_data, int size);
void						delete_list(t_list **head_ref);
void						push_head(t_list **head_ref, void *new_data,
														int size);
void						delete_item(t_list **head, int id);

void						main_loop(t_env *env);

int							get_operation_args(t_arena *arena, int pc,
									t_process *process, int dir_size);
void						get_codage(t_arena *arena, int pc,
							const int *op_codage, t_process *process);
int							move_process(t_process *process);

int							get_ind(t_arena *arena, unsigned int pc);
int							get_dir(t_arena *arena, unsigned int pc,
								int dir_size);
int							get_reg(t_arena *arena, unsigned int pc);
int							get_int(t_arena *arena, unsigned int pc);
short						get_short(t_arena *arena, unsigned int pc);

void						ld(t_env *env, t_process *process);
void						ldi(t_env *env, t_process *process);
void						lld(t_env *env, t_process *process);
void						lldi(t_env *env, t_process *process);

void						add(t_env *env, t_process *process);
void						sub(t_env *env, t_process *process);
void						and(t_env *env, t_process *process);
void						or(t_env *env, t_process *process);
void						xor(t_env *env, t_process *process);

void						zjmp(t_env *env, t_process *process);

void						ffork(t_env *env, t_process *process);
void						lfork(t_env *env, t_process *process);

void						live(t_env *env, t_process *process);
void						aff(t_env *env, t_process *process);

void						st(t_env *env, t_process *process);
void						sti(t_env *env, t_process *process);

void						*memory_n_copy(void *dst, size_t start,
								const void *src, size_t n);
void						swap_union_number(t_number *n);

static	const t_op			g_op_tab[16] =
{
	{live, {T_DIR, 0, 0}, 0, 10, 4},
	{ld, {T_DIR | T_IND, T_REG, 0}, 1, 5, 4},
	{st, {T_REG, T_IND | T_REG, 0}, 1, 5, 4},
	{add, {T_REG, T_REG, T_REG}, 1, 10, 4},
	{sub, {T_REG, T_REG, T_REG}, 1, 10, 4},
	{and, {T_REG | T_DIR | T_IND, T_REG | T_IND | T_DIR, T_REG}, 1, 6, 4},
	{or, {T_REG | T_IND | T_DIR, T_REG | T_IND | T_DIR, T_REG}, 1, 6, 4},
	{xor, {T_REG | T_IND | T_DIR, T_REG | T_IND | T_DIR, T_REG}, 1, 6, 4},
	{zjmp, {T_DIR, 0, 0}, 0, 20, 2},
	{ldi, {T_REG | T_DIR | T_IND, T_DIR | T_REG, T_REG}, 1, 25, 2},
	{sti, {T_REG, T_REG | T_DIR | T_IND, T_DIR | T_REG}, 1, 25, 2},
	{ffork, {T_DIR, 0, 0}, 0, 800, 2},
	{lld, {T_DIR | T_IND, T_REG, 0}, 1, 10, 4},
	{lldi, {T_REG | T_DIR | T_IND, T_DIR | T_REG, T_REG}, 1, 50, 2},
	{lfork, {T_DIR, 0, 0}, 0, 1000, 2},
	{aff, {T_REG, 0, 0}, 1, 2, 4}
};

int							g_last_process_id;

#endif
