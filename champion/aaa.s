.name "AAA"
.comment "zork killer"

start_and_end:
	live		%1
	sti			r1, %:start_and_end, %1
	fork		%:start_and_end
	sub			r16, r16, r16
	zjmp		%:start_and_end
