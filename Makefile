#######  Variables

NAME_ASM = asm

NAME_VM = corewar

CC = gcc

CC_FLAGS = -Wall -Wextra -Werror -fPIC

LIBFT_DIR = libft/

LIBFT_INC_DIR = libft/includes/

LIBFT = libftprintf.a

INC_DIR = ./inc/

INC_VM_HEADER = ./inc/vm.h

INC_OP_HEADER = ./inc/op.h

INC_ASM_HEADER = ./inc/asm.h

OBJ_DIR_ASM = ./obj/asm/

OBJ_DIR_VM = ./obj/vm/

SRC_DIR_ASM = ./src/asm/

SRC_DIR_VM = ./src/vm/

SRC_ASM = 	asm_main.c asm_help_functions.c asm_parse_name_and_comment.c \
			asm_parse_instruction.c write_all.c into_bytecode.c \
			asm_init.c asm_parse_label.c asm_parse_arguments.c \
			asm_parse_arguments2.c asm_help_functions2.c \
			asm_help_functions3.c asm_exit.c

SRC_VM =    init.c vm_main.c debug.c usage.c parse_champ.c free.c parse_args.c work_with_lists.c \
            parse_args2.c main_loop.c bool_and_math_operations.c fork_operations.c \
            jump_operations.c live_and_aff_operations.c load_operations.c set_operations.c \
            get_operation_args.c read_operation_args.c main_loop2.c

OBJ_ASM =   $(addprefix $(OBJ_DIR_ASM), $(SRC_ASM:.c=.o))

OBJ_VM =   $(addprefix $(OBJ_DIR_VM), $(SRC_VM:.c=.o))

#######  End of variables

#######  Rules

all: $(NAME_ASM) $(NAME_VM)

$(NAME_ASM): $(OBJ_ASM)
	@make -sC $(LIBFT_DIR)
	@echo ASM compiling...
	@$(CC) -o $(NAME_ASM) $(OBJ_ASM) $(LIBFT_DIR)$(LIBFT)
	@echo ASM compile done!

$(NAME_VM): $(OBJ_VM)
	@make -sC $(LIBFT_DIR)
	@echo VM compiling...
	@$(CC) -o $(NAME_VM) $(OBJ_VM) $(LIBFT_DIR)$(LIBFT)
	@echo VM compile done!

$(OBJ_DIR_ASM)%.o: %.c $(INC_ASM_HEADER)
	@cc $(CC_FLAGS) -c $< -o $@ -I $(INC_DIR) -I $(LIBFT_INC_DIR)

$(OBJ_DIR_VM)%.o: %.c $(INC_VM_HEADER) $(INC_OP_HEADER)
	@cc $(CC_FLAGS) -c $< -o $@ -I $(INC_DIR) -I $(LIBFT_INC_DIR)

clean:
	@make clean -sC $(LIBFT_DIR)
	@rm -f $(OBJ_ASM)
	@rm -f $(OBJ_VM)

fclean: clean
	@rm -f $(LIBFT_DIR)$(LIBFT)
	@rm -f $(NAME_ASM)
	@rm -f $(NAME_VM)

re: fclean all

vpath %.c	$(SRC_DIR_ASM)               \
            $(SRC_DIR_VM)

####### End of rules
