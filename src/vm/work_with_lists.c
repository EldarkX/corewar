/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_with_lists.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 11:27:45 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/08 11:27:46 by anyvchyk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

t_list			*ft_new_elem(void *data, int size)
{
	t_list		*new_elem;

	new_elem = (t_list *)malloc(sizeof(t_list));
	if (!new_elem)
		return (NULL);
	new_elem->content = data;
	new_elem->content_size = size;
	new_elem->next = NULL;
	return (new_elem);
}

void			push_tail(t_list **head, void *data, int size)
{
	t_list		*tmp;

	if (!*head)
		*head = ft_new_elem(data, size);
	else
	{
		tmp = *head;
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = ft_new_elem(data, size);
	}
}

void			push_head(t_list **head_ref, void *new_data, int size)
{
	t_list		*new_node;

	new_node = ft_new_elem(new_data, size);
	new_node->next = *head_ref;
	*head_ref = new_node;
}

void			delete_list(t_list **head_ref)
{
	t_list		*current;
	t_list		*next;

	current = *head_ref;
	while (current != NULL)
	{
		next = current->next;
		free(current->content);
		free(current);
		current = next;
	}
	*head_ref = NULL;
}

void			delete_item(t_list **head, int id)
{
	t_process	*p;
	t_list		**current;
	t_list		*next;

	current = head;
	while (*current)
	{
		p = (*current)->content;
		if (p->id == id)
		{
			next = (*current)->next;
			free((*current)->content);
			free(*current);
			(*current) = next;
			break ;
		}
		current = &(*current)->next;
	}
}
