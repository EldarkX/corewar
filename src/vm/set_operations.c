/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_operations.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 14:00:55 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 14:00:56 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void				swap_union_number(t_number *n)
{
	unsigned char	c;

	c = n->numbers[0];
	n->numbers[0] = n->numbers[3];
	n->numbers[3] = c;
	c = n->numbers[1];
	n->numbers[1] = n->numbers[2];
	n->numbers[2] = c;
}

void				*memory_n_copy(void *dst, size_t start,
		const void *src, size_t n)
{
	size_t			i;
	size_t			j;

	i = 0;
	while (i < n)
	{
		j = (start + i) % MEM_SIZE;
		((char *)dst)[j] = ((char *)src)[i];
		i++;
	}
	return (dst);
}

void				st(t_env *env, t_process *p)
{
	t_number		n;
	size_t			start;

	if (p->codage[1] == T_REG)
	{
		p->reg[p->args[1]] = p->reg[p->args[0]];
		p->args[1]++;
	}
	else
	{
		p->args[1] = get_short(&env->arena, p->pc + 3);
		n.number = p->reg[p->args[0]];
		swap_union_number(&n);
		start = (size_t)(MEM_SIZE + (p->pc + p->args[1] % IDX_MOD)) % MEM_SIZE;
		memory_n_copy(env->arena.arena, start, n.numbers, 4);
	}
	if (env->debug.d2)
		ft_printf("P %4d | st r%d %d\n", p->id, p->args[0] + 1, p->args[1]);
}

void				sti(t_env *env, t_process *p)
{
	t_number		n;
	size_t			start;
	const int		cod = (env->arena.arena[(p->pc + 1) % MEM_SIZE] >> 4) & 3;

	if (p->codage[1] == T_REG)
		p->args[1] = p->reg[p->args[1]];
	else if (cod == IND_CODE)
		p->args[1] %= IDX_MOD;
	if (p->codage[2] == T_REG)
		p->args[2] = p->reg[p->args[2]];
	if (env->debug.d2)
	{
		ft_printf("P %4d | sti r%d %d %d\n", p->id, p->args[0] + 1,
				p->args[1], p->args[2]);
		ft_printf("       | -> store to %d + %d = %d (with pc and mod %d)\n",
		p->args[1], p->args[2], p->args[1] + p->args[2],
		(p->pc + (p->args[1] + p->args[2]) % IDX_MOD));
	}
	n.number = p->reg[p->args[0]];
	swap_union_number(&n);
	start = (size_t)(MEM_SIZE + (p->pc +
				(p->args[1] + p->args[2]) % IDX_MOD)) % MEM_SIZE;
	memory_n_copy(env->arena.arena, start, n.numbers, 4);
}
