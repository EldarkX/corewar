/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 14:26:02 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/19 13:58:39 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void			debug_show_players_data(t_env *env)
{
	int			i;

	write(1, "Introducing contestants...\n", 27);
	i = 0;
	while (i < MAX_PLAYERS)
	{
		if (env->champion[i])
		{
			ft_printf("* Player %d, weighting %d bytes, \"%s\" (\"%s\") !\n",
				env->champion[i]->id + 1, env->champion[i]->exec_code_size,
				env->champion[i]->name, env->champion[i]->comment);
		}
		i++;
	}
}

void			hex_dump(unsigned char *arena, int len)
{
	int			i;

	i = 0;
	while (i < len)
	{
		if (!(i % 64))
		{
			if (i)
				write(1, " \n", 2);
			ft_printf("0x%04x :", i);
		}
		ft_printf(" %02x", arena[i]);
		i++;
	}
	write(1, " \n", 2);
}

void			set_processes_id(t_env *env)
{
	t_list		*processes;
	t_process	*process;
	int			i;

	processes = env->arena.procs;
	g_last_process_id = env->num_of_champions;
	i = env->num_of_champions;
	while (processes)
	{
		process = processes->content;
		process->id = i;
		processes = processes->next;
		i--;
	}
}

void			set_args_codage_null(t_process *p)
{
	int			i;

	i = -1;
	while (++i < 3)
	{
		p->codage[i] = 0;
		p->args[i] = 0;
	}
}
