/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 13:58:45 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 13:58:47 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void	ffork(t_env *env, t_process *process)
{
	int			new_process_pc;
	int			i;
	t_process	*child_process;

	child_process = init_process();
	new_process_pc = get_short(&env->arena, process->pc + 1);
	child_process->pc = process->pc + new_process_pc % IDX_MOD;
	i = -1;
	while (++i < 16)
		child_process->reg[i] = process->reg[i];
	if (env->debug.d2)
	{
		ft_printf("P %4d | fork %d (%d)\n", process->id,
				new_process_pc, child_process->pc);
	}
	child_process->pc = (MEM_SIZE + child_process->pc) % MEM_SIZE;
	child_process->carry = process->carry;
	child_process->id = ++g_last_process_id;
	child_process->will_live = process->will_live;
	push_head(&env->arena.procs, child_process, sizeof(t_process));
}

void	lfork(t_env *env, t_process *process)
{
	int			new_process_pc;
	int			i;
	t_process	*child_process;

	child_process = init_process();
	new_process_pc = get_short(&env->arena, process->pc + 1);
	child_process->pc = process->pc + new_process_pc;
	i = -1;
	while (++i < 16)
		child_process->reg[i] = process->reg[i];
	if (env->debug.d2)
	{
		ft_printf("P %4d | lfork %d (%d)\n", process->id,
				new_process_pc, child_process->pc);
	}
	child_process->pc = (MEM_SIZE + child_process->pc) % MEM_SIZE;
	child_process->carry = process->carry;
	child_process->id = ++g_last_process_id;
	child_process->will_live = process->will_live;
	push_head(&env->arena.procs, child_process, sizeof(t_process));
}
