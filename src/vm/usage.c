/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/06 13:58:37 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/06 13:58:39 by anyvchyk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void	show_usage_and_exit(void)
{
	ft_printf("USAGE: ./corewar [-dump nbr_cycles] "
	"[[-n number] champion1.cor] champion.cor -d1 -d2 -d3\n");
	ft_printf("        -dump nbr_cycles: VM will dump the "
	"memory on the standard output\n"
	"                      and exit at the end of "
		"nbr_cycles of executions.\n");
	ft_printf("        -n number: sets the number of the next player.\n"
	"                      If non-existent, "
		"the player will have the next\n"
	"                      available number in the order of "
	"the parameters.\n");
	ft_printf("        -d1: shows current cycle and CTD.\n");
	ft_printf("        -d2: shows commands and args.\n");
	ft_printf("        -d2: shows deaths.\n");
	exit(0);
}
