/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <aantropo@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 22:03:02 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 14:00:21 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

static int		check_op_args(t_env *env, t_process *process)
{
	int i;
	int pc_tmp;

	i = 0;
	pc_tmp = (process->pc + 1) % MEM_SIZE;
	get_codage(&env->arena, pc_tmp,
			g_op_tab[process->op_num - 1].args, process);
	while (i < 3)
	{
		if (((process->codage[i] & g_op_tab[process->op_num - 1].args[i]) !=
		process->codage[i]) || (process->codage[i] == 0 &&
		g_op_tab[process->op_num - 1].args[i] != 0))
		{
			pc_tmp = (pc_tmp + 1) % MEM_SIZE;
			get_operation_args(&env->arena, pc_tmp, process,
				g_op_tab[process->op_num - 1].dir_size);
			return (0);
		}
		i++;
	}
	pc_tmp = (pc_tmp + 1) % MEM_SIZE;
	if (!get_operation_args(&env->arena, pc_tmp, process,
		g_op_tab[process->op_num - 1].dir_size))
		return (0);
	return (1);
}

static void		do_operation(t_env *env, t_process *process)
{
	if (g_op_tab[process->op_num - 1].is_have_args)
	{
		if (!check_op_args(env, process))
		{
			process->pc = (process->pc + 2 + process->codage[0] +
					process->codage[1] + process->codage[2]) % MEM_SIZE;
			return ;
		}
		g_op_tab[process->op_num - 1].function(env, process);
	}
	else
		g_op_tab[process->op_num - 1].function(env, process);
	process->pc = (process->pc + move_process(process)) % MEM_SIZE;
}

static void		manage_process(t_env *env, t_process *process)
{
	if (process->cycles_to_do_op > 1)
		process->cycles_to_do_op--;
	else if (process->cycles_to_do_op == 0 && process->op_num == -999999)
	{
		if (env->arena.arena[process->pc] < 1 ||
		env->arena.arena[process->pc] > 16)
			process->pc = (process->pc + 1) % MEM_SIZE;
		else
		{
			process->op_num = env->arena.arena[process->pc];
			process->cycles_to_do_op =
					g_op_tab[process->op_num - 1].cycles_to_do_op - 1;
		}
	}
	else if (process->cycles_to_do_op == 1)
	{
		do_operation(env, process);
		set_args_codage_null(process);
		process->op_num = -999999;
		process->cycles_to_do_op = 0;
	}
}

static void		processes_loop(t_env *env)
{
	t_list		*processes;
	t_process	*process;

	processes = env->arena.procs;
	while (processes)
	{
		process = processes->content;
		manage_process(env, process);
		processes = processes->next;
	}
}

void			main_loop(t_env *env)
{
	long long	cycle;

	cycle = 0;
	while ((!env->dump_mode || env->nbr_cycles--) && env->arena.procs)
	{
		(env->debug.d1) ? ft_printf("It is now cycle %d"
				"\n", ++cycle) : cycle++;
		processes_loop(env);
		if ((env->nbr_cycles_to_checkup == env->cycle_to_die
			|| env->cycle_to_die <= 0) && checkup(env))
			break ;
		env->nbr_cycles_to_checkup++;
	}
	(env->dump_mode) ? hex_dump(env->arena.arena, MEM_SIZE) : 0;
}
