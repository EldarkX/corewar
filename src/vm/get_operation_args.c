/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_operation_args.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 13:59:00 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 13:59:02 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void			get_codage(t_arena *arena, int pc,
		const int *op_codage, t_process *process)
{
	int			i;

	process->codage[0] = (int)((arena->arena[pc] >> 6) & 3);
	process->codage[1] = (int)((arena->arena[pc] >> 4) & 3);
	process->codage[2] = (int)((arena->arena[pc] >> 2) & 3);
	i = 0;
	while (i < 3)
	{
		if (op_codage[i] == 0)
			process->codage[i] = 0;
		else if (process->codage[i] == REG_CODE)
			process->codage[i] = T_REG;
		else if (process->codage[i] == IND_CODE)
			process->codage[i] = T_IND;
		else if (process->codage[i] == DIR_CODE)
			process->codage[i] = T_DIR;
		else
			process->codage[i] = 0;
		i++;
	}
}

int				get_operation_args(t_arena *arena, int pc,
		t_process *process, int dir_size)
{
	int i;

	i = 0;
	while (i < 3)
	{
		if (process->codage[i] == T_REG)
		{
			if ((process->args[i] = get_reg(arena, pc)) < 0)
				return (0);
		}
		else if (process->codage[i] == T_IND)
			process->args[i] = get_ind(arena, pc);
		else if (process->codage[i] == T_DIR)
		{
			process->args[i] = get_dir(arena, pc, dir_size);
			process->codage[i] = dir_size;
		}
		else if (process->codage[i] == 0)
			process->args[i] = 0;
		pc += process->codage[i];
		i++;
	}
	return (1);
}

int				move_process(t_process *process)
{
	if (process->op_num == 1 || process->op_num == 4 || process->op_num == 5)
		return (5);
	if (process->op_num == 2 || process->op_num == 3 || process->op_num == 13)
		return (process->codage[0] + process->codage[1] + 2);
	if (process->op_num == 6 || process->op_num == 7 || process->op_num == 8
		|| process->op_num == 10 || process->op_num == 11
		|| process->op_num == 14)
	{
		return (process->codage[0] + process->codage[1] +
		process->codage[2] + 2);
	}
	if (process->op_num == 12 || process->op_num == 15 || process->op_num == 16)
		return (3);
	if (process->op_num == 9)
		return (0);
	return (1);
}
