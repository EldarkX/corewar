/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 12:32:35 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/07 12:32:39 by anyvchyk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

static void			check_num(t_env *env, char *str)
{
	int				i;

	i = 0;
	while (str[i])
	{
		if (str[i] < '0' || str[i] > '9')
			exit_with_error(env, "string instead of number\n");
		i++;
	}
}

static void			set_dump_size(t_env *env, char *dump_size)
{
	long long		a;

	if (dump_size == NULL)
		exit_with_error(env, "no number of cycles after flag\n");
	check_num(env, dump_size);
	a = ft_atoi(dump_size);
	if (a < 1 || a > 2147483647)
		exit_with_error(env, "wrong number of cycles\n");
	env->nbr_cycles = a;
	env->dump_mode = true;
}

static void			set_champ_with_id(t_env *env,
		char *num_of_champ, char *champ_name)
{
	int				id;

	if (num_of_champ == NULL)
		exit_with_error(env, "champion's id was not entered\n");
	check_num(env, num_of_champ);
	id = ft_atoi(num_of_champ);
	if (id > MAX_PLAYERS || id < 1)
		exit_with_error(env, "entered id is bigger than MAX_PLAYERS\n");
	id--;
	if (env->champion[id] != NULL)
		exit_with_error(env, "id is already taken by another player\n");
	if (champ_name == NULL)
		exit_with_error(env, "champion's name was not entered\n");
	check_name(env, champ_name);
	env->champion[id] = init_champ(id);
	parse_champion(champ_name, id, env);
}

t_list				*main_args_loop(int argc, char **argv, t_env *env)
{
	int				i;
	t_list			*names;

	i = 0;
	names = NULL;
	while (++i < argc)
	{
		if (!ft_strcmp(argv[i], "-dump"))
		{
			i++;
			set_dump_size(env, argv[i]);
			continue ;
		}
		else if (!ft_strcmp(argv[i], "-n"))
		{
			set_champ_with_id(env, argv[i + 1], argv[i + 2]);
			i += 2;
			continue ;
		}
		else
			set_debug_and_other(env, argv[i], &names);
	}
	return (names);
}

void				parse_args(int argc, char **argv, t_env *env)
{
	t_list			*names;

	names = NULL;
	names = main_args_loop(argc, argv, env);
	parse_list_with_names(env, &names);
	delete_list(&names);
	check_if_any_champs_present(env);
}
