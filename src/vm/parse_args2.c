/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 11:49:52 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/08 11:49:53 by anyvchyk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void					check_name(t_env *env, char *name)
{
	int					i;
	int					dots;
	char				*dot_place;

	i = -1;
	dots = 0;
	while (name[++i])
	{
		if (name[i] == '.')
			dots++;
	}
	if (dots != 1)
		exit_with_error(env, "invalid input file name or key\n");
	dot_place = ft_strchr(name, '.');
	if (ft_strcmp(dot_place, ".cor"))
		exit_with_error(env, "wrong extension\n");
}

static int				get_free_id(t_env *env)
{
	int					id;

	id = 0;
	while (id < MAX_PLAYERS)
	{
		if (env->champion[id] == NULL)
			return (id);
		id++;
	}
	exit_with_error(env, "no free id for champion\n");
	return (id);
}

static void				set_champ_without_id(t_env *env, char *champ_name)
{
	int					id;

	check_name(env, champ_name);
	id = get_free_id(env);
	env->champion[id] = init_champ(id);
	parse_champion(champ_name, id, env);
}

void					check_if_any_champs_present(t_env *env)
{
	int					i;

	i = 0;
	while (i < MAX_PLAYERS)
	{
		if (env->champion[i] != NULL)
			return ;
		i++;
	}
	exit_with_error(env, "no champions were defined\n");
}

void					parse_list_with_names(t_env *env, t_list **head)
{
	t_list				*l;

	l = *head;
	while (l)
	{
		set_champ_without_id(env, l->content);
		l = l->next;
	}
}
