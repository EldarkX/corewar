/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_champ.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/06 13:58:19 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/06 13:58:21 by anyvchyk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

static void				check_magic_header(t_env *env, int fd)
{
	unsigned char		magic[4];
	unsigned			result;
	short				read_res;

	read_res = read(fd, magic, 4);
	if (read_res != 4)
		exit_with_error(env, "magic header is missing!\n");
	result = magic[3] | (magic[2] << 8) | (magic[1] << 16) | (magic[0] << 24);
	if (result != COREWAR_EXEC_MAGIC)
		exit_with_error(env, "magic header is wrong\n");
}

static char				*get_line(int fd, int size, t_env *env)
{
	char				*buff;
	int					res;

	buff = (char*)malloc(size);
	res = read(fd, buff, size);
	if (res != size)
		exit_with_error(env, "invalid file which looks like champion\n");
	if (size == 4)
		free(buff);
	return (buff);
}

static unsigned			set_size_of_champs_code(t_env *env, int fd)
{
	unsigned char		byte[4];
	unsigned			result;
	short				read_res;

	read_res = read(fd, byte, 4);
	if (read_res != 4)
		exit_with_error(env, "no 4 bytes dedicated for champion's size!\n");
	result = byte[3] | (byte[2] << 8) | (byte[1] << 16) | (byte[0] << 24);
	if (result > CHAMP_MAX_SIZE)
		exit_with_error(env, "champion's code size is too big\n");
	return (result);
}

void					parse_champion(char *file_name, int id, t_env *env)
{
	int					fd;
	char				check;

	if ((fd = open(file_name, O_RDONLY)) == -1)
		exit_with_error(env, "invalid file!\n");
	check_magic_header(env, fd);
	env->champion[id]->name = get_line(fd, PROG_NAME_LENGTH, env);
	get_line(fd, 4, env);
	env->champion[id]->exec_code_size = set_size_of_champs_code(env, fd);
	env->champion[id]->comment = get_line(fd, COMMENT_LENGTH, env);
	get_line(fd, 4, env);
	env->champion[id]->exec_code =
			get_line(fd, env->champion[id]->exec_code_size, env);
	if ((read(fd, &check, 1)) == 1)
		exit_with_error(env, "champ has different size\n");
	env->num_of_champions++;
}
