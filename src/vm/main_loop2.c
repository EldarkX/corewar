/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/26 17:37:03 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/26 17:37:16 by anyvchyk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

bool			processes_checkup(t_env *env)
{
	t_list		*processes;
	t_process	*process;

	processes = env->arena.procs;
	while (processes)
	{
		process = processes->content;
		if (!process->will_live)
		{
			if (env->debug.d3)
				ft_printf("Process %d is dead.\n", process->id);
			delete_item(&env->arena.procs, process->id);
		}
		else
			process->will_live = false;
		processes = processes->next;
	}
	processes = env->arena.procs;
	if (processes == NULL)
		return (1);
	return (0);
}

bool			checkup(t_env *env)
{
	env->nbr_checks++;
	if (env->nbr_checks == MAX_CHECKS || env->nbr_live >= NBR_LIVE)
	{
		env->cycle_to_die -= CYCLE_DELTA;
		(env->debug.d1) ? ft_printf("Cycle to die is now %d\n",
				env->cycle_to_die) : 0;
		env->nbr_checks = 0;
	}
	env->nbr_live = 0;
	env->nbr_cycles_to_checkup = 0;
	return (processes_checkup(env));
}
