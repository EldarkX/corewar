/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   live_and_aff_operations.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 13:59:34 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 13:59:35 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void			live(t_env *env, t_process *process)
{
	const int	save = get_dir(&env->arena, process->pc + 1, 4);
	int			player_num;

	if (env->debug.d2)
		ft_printf("P %4d | live %d\n", process->id, save);
	player_num = save * -1;
	if (player_num > 0 && player_num <= MAX_PLAYERS)
	{
		if (env->champion[player_num - 1] != NULL)
		{
			ft_printf("Player %d (%s) is said to be alive\n",
					player_num, env->champion[player_num - 1]->name);
			env->winner = env->champion[player_num - 1];
		}
	}
	process->will_live = 1;
	env->nbr_live++;
}

void			aff(t_env *env, t_process *process)
{
	const int	reg = process->args[0];

	if (reg >= 0 && reg < 16)
	{
		if (env->debug.d2)
			ft_printf("P    4 | aff ");
		ft_printf("%c\n", process->reg[reg]);
	}
	env = env + (1 - 1);
}
