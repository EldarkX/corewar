/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jump_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 13:59:14 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 13:59:15 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void	zjmp(t_env *env, t_process *p)
{
	int target_pc;

	target_pc = get_short(&env->arena, p->pc + 1);
	if (env->debug.d2)
		ft_printf("P %4d | zjmp %d ", p->id, target_pc);
	if (p->carry == 1)
	{
		p->pc = (MEM_SIZE + p->pc + target_pc % IDX_MOD) % MEM_SIZE;
		if (env->debug.d2)
			ft_printf("OK\n");
	}
	else
	{
		p->pc = (p->pc + 3) % MEM_SIZE;
		if (env->debug.d2)
			ft_printf("FAILED\n");
	}
}
