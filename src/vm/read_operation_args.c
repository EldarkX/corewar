/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_operation_args.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 14:00:48 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 14:00:49 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/vm.h"

int				get_int(t_arena *arena, unsigned int pc)
{
	t_number	n;

	pc = (MEM_SIZE + pc) % MEM_SIZE;
	n.numbers[0] = arena->arena[(pc + 3) % MEM_SIZE];
	n.numbers[1] = arena->arena[(pc + 2) % MEM_SIZE];
	n.numbers[2] = arena->arena[(pc + 1) % MEM_SIZE];
	n.numbers[3] = arena->arena[(pc) % MEM_SIZE];
	return (n.number);
}

short			get_short(t_arena *arena, unsigned int pc)
{
	t_number	n;

	pc = (MEM_SIZE + pc) % MEM_SIZE;
	n.numbers[0] = arena->arena[(pc + 1) % MEM_SIZE];
	n.numbers[1] = arena->arena[pc % MEM_SIZE];
	n.numbers[2] = 0;
	n.numbers[3] = 0;
	return ((short)n.number);
}

int				get_ind(t_arena *arena, unsigned int pc)
{
	int		value;
	short	pc2;

	pc2 = get_short(arena, pc);
	value = get_int(arena, pc2);
	return (value);
}

int				get_dir(t_arena *arena, unsigned int pc, int dir_size)
{
	int	value;

	if (dir_size == 2)
		value = get_short(arena, pc);
	else
		value = get_int(arena, pc);
	return (value);
}

int				get_reg(t_arena *arena, unsigned int pc)
{
	int value;

	pc = (MEM_SIZE + pc) % MEM_SIZE;
	value = arena->arena[pc];
	if (value < 1 || value > REG_NUMBER)
		return (-1);
	return (value - 1);
}
