/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asabanov <asabanov@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 22:03:02 by asabanov          #+#    #+#             */
/*   Updated: 2019/10/03 08:58:43 by anyvchyk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

static void			init_winner(t_env *env)
{
	int				i;

	i = 0;
	while (i < MAX_PLAYERS)
	{
		if (env->champion[i])
		{
			env->winner = env->champion[i];
			return ;
		}
		i++;
	}
}

void				set_debug_and_other(t_env *env,
										char *argv, t_list **names)
{
	if (!ft_strcmp(argv, "-d1"))
		env->debug.d1 = 1;
	else if (!ft_strcmp(argv, "-d2"))
		env->debug.d2 = 1;
	else if (!ft_strcmp(argv, "-d3"))
		env->debug.d3 = 1;
	else
		push_tail(names, ft_strdup(argv), sizeof(argv));
}

int					main(int argc, char **argv)
{
	t_env			env;

	if (argc < 2 || CYCLE_TO_DIE < 0 || CYCLE_TO_DIE > 100000)
		show_usage_and_exit();
	init_env(&env);
	parse_args(argc, argv, &env);
	debug_show_players_data(&env);
	init_arena_and_processes(&env);
	init_winner(&env);
	main_loop(&env);
	if ((env.nbr_cycles > -1 && env.dump_mode) || !env.dump_mode)
		ft_printf("Player %d (%s) won\n",
			env.winner->id + 1, env.winner->name);
	return (0);
}
