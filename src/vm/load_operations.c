/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 13:59:44 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 13:59:45 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void			ld(t_env *env, t_process *p)
{
	if (env->debug.d2)
		ft_printf("P %4d | ld %d r%d\n", p->id, p->args[0], p->args[1] + 1);
	p->reg[p->args[1]] = p->args[0];
	p->carry = p->reg[p->args[1]] ? 0 : 1;
}

void			lld(t_env *env, t_process *p)
{
	if (env->debug.d2)
	{
		ft_printf("P %4d | lld %d r%d\n", p->id,
				p->args[0], p->args[1] + 1);
	}
	p->reg[p->args[1]] = p->args[0];
	p->carry = p->reg[p->args[1]] ? 0 : 1;
}

static void		debug_ldi(t_process *p, char *s)
{
	ft_printf("P %4d | %s %d %d r%d\n"
	"       | -> load from %d + %d = %d (with pc and mod %d)\n",
	p->id, s, p->args[0], p->args[1], p->args[2] + 1, p->args[0], p->args[1],
	p->args[0] + p->args[1], ((p->args[0] + p->args[1]) % IDX_MOD) + p->pc);
}

void			ldi(t_env *env, t_process *p)
{
	int			tmp_pc;
	int			i;

	i = env->arena.arena[p->pc + 1] >> 6;
	if (p->codage[0] == T_REG)
		p->args[0] = p->reg[p->args[0]];
	if (i == IND_CODE)
	{
		p->args[0] = (get_short(&env->arena, p->pc + 2)
							% IDX_MOD) + p->pc;
		p->args[0] = get_int(&env->arena, p->args[0]);
	}
	if (p->codage[1] == T_REG)
		p->args[1] = p->reg[p->args[1]];
	if (env->debug.d2)
		debug_ldi(p, "ldi");
	tmp_pc = get_dir(&env->arena, p->pc + ((p->args[0] +
			p->args[1]) % IDX_MOD), 4);
	p->reg[p->args[2]] = tmp_pc;
}

void			lldi(t_env *env, t_process *p)
{
	int			tmp_pc;
	int			i;

	i = env->arena.arena[p->pc + 1] >> 6;
	if (p->codage[0] == T_REG)
		p->args[0] = p->reg[p->args[0]];
	if (i == IND_CODE)
	{
		p->args[0] = (get_short(&env->arena, p->pc + 2)
							% IDX_MOD) + p->pc;
		p->args[0] = get_int(&env->arena, p->args[0]);
	}
	if (p->codage[1] == T_REG)
		p->args[1] = p->reg[p->args[1]];
	if (env->debug.d2)
		debug_ldi(p, "lldi");
	tmp_pc = get_dir(&env->arena, p->pc + ((p->args[0] +
	p->args[1])), 4);
	p->reg[p->args[2]] = tmp_pc;
	p->carry = p->reg[p->args[2]] ? 0 : 1;
}
