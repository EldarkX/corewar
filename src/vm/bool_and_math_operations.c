/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bool_and_math_operations.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 13:58:20 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/19 13:58:22 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void			add(t_env *env, t_process *p)
{
	env = env + (1 - 1);
	p->reg[p->args[2]] = p->reg[p->args[0]] + p->reg[p->args[1]];
	p->carry = (!p->reg[p->args[2]]) ? 1 : 0;
	if (env->debug.d2)
	{
		ft_printf("P %4d | add r%d r%d r%d\n", p->id,
				p->args[0] + 1, p->args[1] + 1, p->args[2] + 1);
	}
}

void			sub(t_env *env, t_process *p)
{
	env = env + (1 - 1);
	p->reg[p->args[2]] = p->reg[p->args[0]] - p->reg[p->args[1]];
	p->carry = (!p->reg[p->args[2]]) ? 1 : 0;
	if (env->debug.d2)
	{
		ft_printf("P %4d | sub r%d r%d r%d\n", p->id,
				p->args[0] + 1, p->args[1] + 1, p->args[2] + 1);
	}
}

void			and(t_env *env, t_process *p)
{
	if (p->codage[0] == T_REG)
		p->args[0] = p->reg[p->args[0]];
	if (p->codage[1] == T_REG)
		p->args[1] = p->reg[p->args[1]];
	if (env->debug.d2)
	{
		ft_printf("P %4d | and %d %d r%d\n", p->id,
				p->args[0], p->args[1], p->args[2] + 1);
	}
	p->reg[p->args[2]] = p->args[0] & p->args[1];
	p->carry = p->reg[p->args[2]] == 0 ? 1 : 0;
}

void			or(t_env *env, t_process *p)
{
	if (p->codage[0] == T_REG)
		p->args[0] = p->reg[p->args[0]];
	if (p->codage[1] == T_REG)
		p->args[1] = p->reg[p->args[1]];
	if (env->debug.d2)
	{
		ft_printf("P %4d | or %d %d r%d\n", p->id,
				p->args[0], p->args[1], p->args[2] + 1);
	}
	p->reg[p->args[2]] = p->args[0] | p->args[1];
	p->carry = p->reg[p->args[2]] == 0 ? 1 : 0;
}

void			xor(t_env *env, t_process *p)
{
	if (p->codage[0] == T_REG)
		p->args[0] = p->reg[p->args[0]];
	if (p->codage[1] == T_REG)
		p->args[1] = p->reg[p->args[1]];
	p->reg[p->args[2]] = p->args[0] ^ p->args[1];
	p->carry = p->reg[p->args[2]] == 0 ? 1 : 0;
	if (env->debug.d2)
	{
		ft_printf("P %4d | xor %d %d r%d\n", p->id,
				p->args[0], p->args[1], p->args[2] + 1);
	}
}
