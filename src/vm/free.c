/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/06 16:02:29 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/19 13:58:54 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void		free_champ(t_champion *champ)
{
	if (champ == NULL)
		return ;
	if (champ->name != NULL)
		free(champ->name);
	if (champ->comment != NULL)
		free(champ->comment);
	if (champ->exec_code != NULL)
		free(champ->exec_code);
	free(champ);
	champ = NULL;
}

void		free_all(t_env *env)
{
	int		i;

	if (env->arena.procs != NULL)
		delete_list(&env->arena.procs);
	i = 0;
	while (i < MAX_PLAYERS)
	{
		if (env->champion[i] != NULL)
			free_champ(env->champion[i]);
		i++;
	}
}

void		exit_with_error(t_env *env, char *error_text)
{
	write(2, "ERROR: ", 7);
	write(2, error_text, ft_strlen(error_text));
	free_all(env);
	exit(1);
}
