/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 12:29:52 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/19 13:59:08 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/vm.h"

void					init_env(t_env *env)
{
	int					i;

	env->nbr_checks = 0;
	env->nbr_cycles_to_checkup = 1;
	env->nbr_live = 0;
	env->debug.d1 = 0;
	env->debug.d2 = 0;
	env->debug.d3 = 0;
	env->dump_mode = 0;
	env->nbr_cycles = -1;
	env->num_of_champions = 0;
	env->arena.procs = NULL;
	env->cycle_to_die = CYCLE_TO_DIE;
	i = 0;
	while (i < MAX_PLAYERS)
	{
		env->champion[i] = NULL;
		i++;
	}
	i = -1;
	while (++i < MEM_SIZE)
		env->arena.arena[i] = 0;
}

t_champion				*init_champ(int id)
{
	t_champion			*champ;

	champ = (t_champion*)malloc(sizeof(t_champion));
	champ->id = id;
	champ->name = NULL;
	champ->comment = NULL;
	champ->exec_code = NULL;
	return (champ);
}

static void				*ft_memcpyfrom
	(void *dst, const void *src, size_t n, size_t start)
{
	size_t				i;

	i = 0;
	while (i < n)
	{
		((char*)dst)[start] = ((char*)src)[i];
		start++;
		i++;
	}
	return (dst);
}

void					init_arena_and_processes(t_env *env)
{
	int					i;
	int					players_pos;

	i = 0;
	players_pos = 0;
	while (i < MAX_PLAYERS)
	{
		if (env->champion[i] != NULL)
		{
			ft_memcpyfrom(env->arena.arena,
					env->champion[i]->exec_code,
					env->champion[i]->exec_code_size,
					players_pos);
			push_head(&env->arena.procs, init_process(), sizeof(t_process));
			((t_process *)env->arena.procs->content)->pc = players_pos;
			((t_process *)env->arena.procs->content)->reg[0] = PLAYER_CODE - i;
			players_pos += MEM_SIZE / env->num_of_champions;
		}
		i++;
	}
	set_processes_id(env);
}

t_process				*init_process(void)
{
	t_process			*proc;
	int					i;

	proc = (t_process*)malloc(sizeof(t_process));
	proc->pc = 0;
	proc->carry = 0;
	proc->will_live = false;
	proc->cycles_to_do_op = 0;
	proc->op_num = -999999;
	i = -1;
	while (++i < 16)
		proc->reg[i] = 0;
	i = 0;
	while (i < 3)
	{
		proc->codage[i] = 0;
		proc->args[i] = 0;
		i++;
	}
	return (proc);
}
