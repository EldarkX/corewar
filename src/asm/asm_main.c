/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asabanov <asabanov@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 22:01:05 by asabanov          #+#    #+#             */
/*   Updated: 2019/10/25 19:26:40 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

static int		g_file_number;

static int		check_file_name(t_asm *a, char *file_name)
{
	unsigned int	i;
	char			file_name_cor[ft_strlen(file_name) + 3];

	if (ft_strlen(file_name) < 3 || !ft_strchr(file_name, '.'))
		return (0);
	i = -1;
	while (file_name[++i] != '.')
		file_name_cor[i] = file_name[i];
	file_name_cor[i] = '.';
	if (++i >= ft_strlen(file_name) || file_name[i] != 's')
		return (0);
	file_name_cor[i] = 'c';
	if (++i != ft_strlen(file_name))
		return (0);
	file_name_cor[i++] = 'o';
	file_name_cor[i++] = 'r';
	file_name_cor[i++] = '\0';
	a->file_name_cor = ft_strdup(file_name_cor);
	return (1);
}

int				main(int ac, char **av)
{
	t_asm		*a;

	if (ac == 1)
		asm_exit(NULL, 1, "USAGE:\t ./asm champion_name.s ...");
	g_file_number = 0;
	while (++g_file_number < ac)
	{
		a = asm_init();
		if ((a->fd = open(av[g_file_number], O_RDONLY)) < 0)
		{
			ft_printf("Error: cant open input file (%s).\n", av[g_file_number]);
			asm_free_memory(a);
			continue ;
		}
		if (!check_file_name(a, av[g_file_number]))
		{
			ft_printf("Error: bad file extension (%s).\n", av[g_file_number]);
			asm_free_memory(a);
			continue ;
		}
		asm_get_name_and_comment(a);
		write_whole_bytecode(a);
		asm_free_memory(a);
	}
	return (0);
}
