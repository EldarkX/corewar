/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_parse_arguments2.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 19:07:45 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 19:07:46 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

int		check_arg(t_instruction *instruction, int id)
{
	int		i;
	t_arg	*arg;

	if (id == 1)
		arg = instruction->arg1;
	else if (id == 2)
		arg = instruction->arg2;
	else
		arg = instruction->arg3;
	i = instruction->opcode - 1;
	if ((arg->type & g_op_tab[i].args[id - 1])
		!= arg->type)
	{
		if (arg->type == T_REG)
			return (0);
		else if (arg->type == T_DIR)
			return (0);
		return (0);
	}
	if (arg->type == T_DIR)
		arg->len = (unsigned char)g_op_tab[i].dir_size;
	if (arg->type == T_DIR && arg->len == 2)
		if ((int)arg->value > 65535)
			return (0);
	return (1);
}

int		parse_next_arg(t_asm *a,
		t_instruction *inst, int id)
{
	int i;

	if (a->line[a->i_of_code] != SEPARATOR_CHAR)
		return (0);
	++a->i_of_code;
	if ((i = skip_useless(a)) == -1)
		return (0);
	i = strisword(a->line, i);
	if (!parse_arg(a, inst, ft_strsub(a->line,
			(unsigned int)a->i_of_code,
			(size_t)(i - (int)a->i_of_code)), id))
		return (0);
	a->i_of_code = i;
	return (1);
}
