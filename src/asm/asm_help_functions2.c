/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_help_functions2.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 18:59:41 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 18:59:42 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

long int		ft_atol(char *string)
{
	long int		number;
	unsigned int	digit;
	int				sign;

	sign = 0;
	number = 0;
	digit = 0;
	if (*string == '-')
	{
		sign++;
		string++;
	}
	while (digit <= 9 && *string && ft_isdigit(*string))
	{
		digit = *string - '0';
		number = number * 10 + digit;
		string++;
	}
	return (sign ? -number : number);
}

unsigned int	digit_count(long long n)
{
	long long	rez;

	rez = n <= 0 ? 1 : 0;
	while (n)
	{
		rez++;
		n /= 10;
	}
	return ((unsigned int)rez);
}

int				ft_isblank(char c)
{
	if (c == 32 || c == 9)
		return (1);
	return (0);
}

int				check_label_content(char label_letter)
{
	if (ft_strchr(LABEL_CHARS, label_letter))
		return (1);
	return (0);
}
