/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_parse_arguments.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 19:05:02 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 19:05:04 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

static void			parse_t_reg(t_asm *a,
		t_arg **arg, char *line)
{
	unsigned int	value;

	value = ft_atol(line + 1);
	if ((*(line + 1) && *(line + 1) == LABEL_CHAR) ||
			(*(line + 2) && *(line + 2) == LABEL_CHAR))
	{
		ft_strdel(&line);
		ft_strdel(&a->line);
		asm_exit(a, 1, "Bad T_REG argument.");
	}
	if (value < 1 || value > REG_NUMBER)
	{
		ft_strdel(&line);
		ft_strdel(&a->line);
		asm_exit(a, 1, "Bad T_REG argument.");
	}
	*arg = arg_init(REG_CODE, 0, value, NULL);
	a->i_of_code += ft_strlen(line);
}

static void			parse_t_dir(t_asm *a,
		t_arg **arg, char *line)
{
	unsigned int	value;
	char			label_flag;
	char			*label;
	int				i;

	label = NULL;
	i = 1;
	while (ft_isdigit(*(line + i)) && line[i] == '0' && line[i + 1])
		++i;
	value = ft_atol(line + 1);
	label_flag = (*(line + 1) == LABEL_CHAR) ? (char)1 : (char)0;
	if ((!label_flag && digit_count(value) < ft_strlen(line + 1) - i) ||
		(!label_flag && !ft_isdigit((*(line + 1))) && *(line + 1) != '-') ||
		(!label_flag && (!value && !ft_isdigit(*(line + 1)))))
	{
		ft_strdel(&line);
		ft_strdel(&a->line);
		asm_exit(a, 1, "Bad T_DIR argument.");
	}
	if (label_flag)
		label = ft_strdup(line + 2);
	*arg = arg_init(DIR_CODE, label_flag, value, label);
	a->i_of_code += ft_strlen(line);
}

static void			parse_t_ind(t_asm *a,
		t_arg **arg, char *line)
{
	unsigned int	value;
	char			label_flag;
	char			*label;
	int				i;

	label = NULL;
	i = 1;
	while (ft_isdigit(*(line + i)) && line[i] == '0' && (*(line + i + 1)))
		++i;
	value = ft_atol(line);
	label_flag = (*line == LABEL_CHAR) ? 1 : 0;
	if ((!label_flag && digit_count((int)value) < ft_strlen(line) - i) ||
		(!value && !ft_isdigit(*line) && !label_flag)
		|| (int)value > 65535)
	{
		ft_strdel(&line);
		ft_strdel(&a->line);
		asm_exit(a, 1, "Bad T_IND argument.");
	}
	if (label_flag)
		label = ft_strdup(line + 1);
	*arg = arg_init(IND_CODE, label_flag, value, label);
	a->i_of_code += ft_strlen(line);
}

int					parse_arg(t_asm *a,
		t_instruction *inst, char *line, int id)
{
	t_arg **arg;

	if (id == 1)
		arg = &inst->arg1;
	else if (id == 2)
		arg = &inst->arg2;
	else
		arg = &inst->arg3;
	if (*line == 'r')
		parse_t_reg(a, arg, line);
	else if (*line == DIRECT_CHAR)
		parse_t_dir(a, arg, line);
	else if (ft_isdigit(*line) || *line == LABEL_CHAR || *line == '-')
		parse_t_ind(a, arg, line);
	else
	{
		ft_strdel(&line);
		return (0);
	}
	ft_strdel(&line);
	return (1);
}

int					parse_arguments(t_asm *a,
						t_instruction *inst, int i)
{
	if ((i = skip_useless(a)) == -1)
		return (0);
	i = strisword(a->line, i);
	if (!parse_arg(a, inst, ft_strsub(a->line, a->i_of_code,
			(i - a->i_of_code)), 1) || !check_arg(inst, 1))
		return (0);
	if (skip_useless(a) > 0)
	{
		if (!parse_next_arg(a, inst, 2) || !check_arg(inst, 2))
			return (0);
		if (skip_useless(a) > 0)
		{
			if (!parse_next_arg(a, inst, 3) || !check_arg(inst, 3))
				return (0);
		}
		if ((skip_useless(a)) != -1)
			return (0);
	}
	if ((!inst->arg2 && g_op_tab[inst->opcode - 1].args_num > 1)
		|| (!inst->arg3 && g_op_tab[inst->opcode - 1].args_num > 2))
		return (0);
	return (1);
}
