/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_parse_label.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 19:31:22 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 19:31:25 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

static int	find_label(t_list **instruction, char *label_name)
{
	t_list			*tmp;
	t_instruction	*i;
	t_list			*label;

	tmp = *instruction;
	while (tmp)
	{
		i = (t_instruction *)tmp->content;
		label = i->labels;
		while (label)
		{
			if (ft_strequ(((t_label *)label->content)->name, label_name))
				return (i->hex_id_for_write);
			label = label->next;
		}
		tmp = tmp->next;
	}
	return (-1);
}

int			check_labels(t_list **inst, int l_index, t_list *tmp,
		t_instruction *i)
{
	tmp = *inst;
	while (tmp)
	{
		i = (t_instruction *)tmp->content;
		if (i->arg1 && i->arg1->label_flag)
		{
			if ((l_index = find_label(inst, i->arg1->label_name)) == -1)
				return (0);
			i->arg1->value = (unsigned short)l_index - i->hex_id_for_write;
		}
		if (i->arg2 && i->arg2->label_flag)
		{
			if ((l_index = find_label(inst, i->arg2->label_name)) == -1)
				return (0);
			i->arg2->value = (unsigned short)l_index - i->hex_id_for_write;
		}
		if (i->arg3 && i->arg3->label_flag)
		{
			if ((l_index = find_label(inst, i->arg3->label_name)) == -1)
				return (0);
			i->arg3->value = (unsigned short)l_index - i->hex_id_for_write;
		}
		tmp = tmp->next;
	}
	return (1);
}

static char	*get_label(t_asm *a, char *label_name)
{
	t_list			*instruction;
	t_list			*label;

	instruction = a->instructions;
	while (instruction)
	{
		label = ((t_instruction *)instruction->content)->labels;
		while (label)
		{
			if (ft_strequ(((t_label *)label->content)->name, label_name))
			{
				ft_strdel(&label_name);
				return (NULL);
			}
			label = label->next;
		}
		instruction = instruction->next;
	}
	return (label_name);
}

static int	parse_label(t_asm *a)
{
	char	*tmp;
	char	*label_name;
	size_t	i;

	i = a->i_of_code;
	tmp = ft_strchr(a->line, LABEL_CHAR);
	if (!tmp || tmp == a->line + i)
		return (-1);
	while ((a->line + i) != tmp)
		if (!check_label_content(a->line[i++]))
			return (0);
	label_name = get_label(a,
			ft_strsub(a->line, (unsigned int)a->i_of_code,
			(size_t)(tmp - (a->line + a->i_of_code))));
	if (!label_name)
		return (-1);
	a->i_of_code += ft_strlen(label_name) + 1;
	ft_lstadd(&get_last_instruction(a)->labels,
			ft_lstnew(label_init(label_name), sizeof(t_label)));
	ft_strdel(&label_name);
	return (1);
}

int			parse_labels(t_asm *a)
{
	int i;

	if ((i = skip_useless(a)) == -1)
		return (-1);
	if (!check_label_content2(a, &i))
		return (-1);
	i = parse_label(a);
	if (!i)
		return (0);
	else if (i == -1)
		return (-1);
	while (skip_useless(a) == -1 && a->line && ft_strlen(a->line) > 0)
	{
		skip_useless_area(a);
		if (!check_label_content2(a, &i))
			return (-1);
		if (!parse_label(a))
			return (0);
	}
	return (1);
}
