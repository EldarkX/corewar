/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_parce_instruction.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/25 19:27:21 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 19:08:26 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

static int		count_codage(t_instruction *i)
{
	int		codage;

	codage = 0;
	if (g_op_tab[i->opcode - 1].is_have_args)
		codage = (((((i->arg1) ? i->arg1->code : 0) << 6) |
						((i->arg2) ? i->arg2->code : 0) << 4 |
						((i->arg3) ? i->arg3->code : 0) << 2));
	return (codage);
}

int				check_instruction_name(char *name)
{
	int		i;

	i = 0;
	while (i < 16)
	{
		if (ft_strequ(name, g_op_tab[i++].name))
			return (i);
	}
	return (0);
}

static int		new_instruction(t_asm *a, char *name)
{
	t_instruction	*inst;
	int				op_code;

	op_code = check_instruction_name(name);
	if (!op_code)
		asm_exit(a, 1, "Bad operation name.");
	inst = get_last_instruction(a);
	inst->name = name;
	inst->opcode = op_code;
	a->i_of_code += ft_strlen(name);
	if (!parse_arguments(a, inst, 0))
	{
		ft_strdel(&a->line);
		asm_exit(a, 1, "Bad arguments.");
	}
	ft_strdel(&a->line);
	inst->codage = count_codage(inst);
	return (1);
}

static void		parse_instruction(t_asm *a)
{
	int		i;
	int		len;
	char	*instruction_name;

	if (skip_useless(a) == -1)
		skip_useless_area(a);
	i = a->i_of_code;
	len = (int)ft_strlen(a->line);
	while (i < len && !ft_isblank(a->line[i]) &&
			a->line[i] != DIRECT_CHAR)
		i++;
	instruction_name = ft_strsub(a->line, a->i_of_code,
			i - a->i_of_code);
	if (!new_instruction(a, instruction_name))
		asm_exit(a, 1, "Bad instruction.");
}

void			asm_parse_insts(t_asm *a)
{
	int i;

	if (!a->line || ft_strlen(a->line) == 0)
		asm_exit(a, 1, "There aren't any instructions.");
	while (a->line && ft_strlen(a->line) > 0)
	{
		push_tail(&a->instructions,
			init_instruction(a), sizeof(t_instruction));
		i = parse_labels(a);
		if (!i)
			asm_exit(a, 1, "Bad labels.");
		if (!a->line || !ft_strlen(a->line))
			break ;
		parse_instruction(a);
		skip_useless_area(a);
	}
	if (!check_labels(&a->instructions, 0, NULL, NULL))
		asm_exit(a, 1, "Bad labels links.");
	get_prog_size(a, &a->instructions);
}
