/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_help_functions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/25 19:26:50 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/25 19:26:52 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

int				skip_useless(t_asm *a)
{
	int len;

	len = ft_strlen(a->line);
	while (a->i_of_code < len &&
		(a->line[a->i_of_code] == ' '
			|| a->line[a->i_of_code] == '\t'))
		++a->i_of_code;
	if (a->i_of_code >= len
		|| a->line[a->i_of_code] == COMMENT_CHAR
		|| a->line[a->i_of_code] == ';')
		return (-1);
	return (a->i_of_code);
}

void			skip_useless_area(t_asm *a)
{
	int	ret;

	a->i_of_code = 0;
	ft_strdel(&a->line);
	ret = get_next_line(a->fd, &a->line);
	++a->line_number;
	while (ret == 1 && (ft_strlen(a->line) == 0 || *a->line
		== COMMENT_CHAR ||
		(skip_useless(a) == -1) || *a->line == ';'))
	{
		ft_strdel(&a->line);
		a->i_of_code = 0;
		ret = get_next_line(a->fd, &a->line);
		++a->line_number;
	}
}

void			skip_whitespaces_and_comments(t_asm *a, char **line)
{
	size_t len;
	size_t i;

	*line = NULL;
	while (get_next_line(a->fd, line) > 0
		&& ++a->line_number)
	{
		i = 0;
		len = ft_strlen(*line);
		while (i < len)
		{
			if ((*line)[i] == ' ' || (*line)[i] == '\t')
				i++;
			else if ((*line)[i] == COMMENT_CHAR || (*line)[i] == ';')
				break ;
			else
				return ;
		}
		ft_strdel(line);
	}
}

t_instruction	*get_last_instruction(t_asm *a)
{
	t_list *tmp;

	tmp = a->instructions;
	if (!tmp)
		return (NULL);
	if (!tmp->next)
		return ((t_instruction *)tmp->content);
	while (tmp->next)
		tmp = tmp->next;
	return ((t_instruction *)tmp->content);
}

int				strisword(const char *line, int start)
{
	int i;

	i = start;
	while (line[i] && line[i] != ' ' && line[i] != '\t' &&
			line[i] != SEPARATOR_CHAR)
		++i;
	return (i);
}
