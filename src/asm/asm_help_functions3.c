/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_help_functions3.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 19:10:57 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 19:10:59 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

t_list			*ft_new_elem(void *data, int size)
{
	t_list		*new_elem;

	new_elem = (t_list *)malloc(sizeof(t_list));
	if (!new_elem)
		return (NULL);
	new_elem->content = data;
	new_elem->content_size = size;
	new_elem->next = NULL;
	return (new_elem);
}

void			push_tail(t_list **head, void *data, int size)
{
	t_list		*tmp;

	if (!*head)
		*head = ft_new_elem(data, size);
	else
	{
		tmp = *head;
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = ft_new_elem(data, size);
	}
}

int				check_label_content2(t_asm *a, int *i)
{
	if (ft_strchr(a->line, LABEL_CHAR))
	{
		while (check_label_content(a->line[*i]))
			(*i)++;
		if (a->line[*i] != ':')
			return (0);
	}
	return (1);
}
