/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_all.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anyvchyk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 19:06:57 by anyvchyk          #+#    #+#             */
/*   Updated: 2019/10/28 19:06:58 by anyvchyk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

void				get_prog_size(t_asm *a, t_list **head)
{
	t_list			*l;
	t_instruction	*i;
	int				size;

	l = *head;
	size = 0;
	while (l)
	{
		i = l->content;
		if (i && i->opcode >= 1 && i->opcode <= 16 && i->arg1)
		{
			size += 1 + ((i->codage) ? 1 : 0) + (i->arg1 ? i->arg1->len : 0);
			size += (i->arg2) ? i->arg2->len : 0;
			size += (i->arg3) ? i->arg3->len : 0;
		}
		l = l->next;
	}
	a->champ_header.prog_size = size;
}

static int			create_file_and_write_magic_header(t_asm *a, char *buff)
{
	int			fd;
	int			mg;

	fd = open(a->file_name_cor, O_WRONLY | O_CREAT | O_TRUNC,
			S_IREAD | S_IWRITE);
	if (fd < 0)
		asm_exit(a, 1, "Can't create .cor file");
	mg = COREWAR_EXEC_MAGIC;
	int_to_bytecode(buff, 0, mg, 4);
	return (fd);
}

void				arg_to_bytecode(char *buff, t_arg *arg, int *pos)
{
	if (arg->len == 1)
	{
		int_to_bytecode(buff, *pos, (unsigned int)arg->value, 1);
		*pos = *pos + 1;
	}
	else if (arg->len == 2)
	{
		int_to_bytecode(buff, *pos, (unsigned int)arg->value, 2);
		*pos = *pos + 2;
	}
	else
	{
		int_to_bytecode(buff, *pos, (unsigned int)arg->value, 4);
		*pos = *pos + 4;
	}
}

void				add_instructions(t_list **head, char *buff, int len)
{
	t_list			*l;
	t_instruction	*i;
	int				pos;

	l = *head;
	pos = PROG_NAME_LENGTH + COMMENT_LENGTH + 16;
	while (l && pos < len)
	{
		i = l->content;
		int_to_bytecode(buff, pos, i->opcode, 1);
		pos += 1;
		if (i->codage)
		{
			int_to_bytecode(buff, pos, (unsigned int)i->codage, 1);
			pos++;
		}
		if (i->arg1)
			arg_to_bytecode(buff, i->arg1, &pos);
		if (i->arg2)
			arg_to_bytecode(buff, i->arg2, &pos);
		if (i->arg3)
			arg_to_bytecode(buff, i->arg3, &pos);
		l = l->next;
	}
}

void				write_whole_bytecode(t_asm *a)
{
	const int		s = a->champ_header.prog_size;
	const int		len = PROG_NAME_LENGTH + COMMENT_LENGTH + 16 + s;
	char			buff[len];
	int				fd;

	ft_bzero(buff, (size_t)len);
	fd = create_file_and_write_magic_header(a, buff);
	ft_memcpy(&buff[4], a->champ_header.prog_name,
			ft_strlen(a->champ_header.prog_name));
	int_to_bytecode(buff, 136, a->champ_header.prog_size, 4);
	ft_memcpy(&buff[140], a->champ_header.comment,
			ft_strlen(a->champ_header.comment));
	add_instructions(&a->instructions, buff, len);
	write(fd, buff, len);
	ft_printf("Writing output to file %s\n", a->file_name_cor);
	close(fd);
}
