/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_parse_name_and_comment.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/25 19:27:00 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/25 19:27:01 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

static size_t	g_index;
static int		g_index2;
static bool		g_is_name_first;
static bool		g_error;

static void		check_signature(t_asm *a, char **line)
{
	if (ft_strnstr((*line) + g_index, NAME_CMD_STRING,
			ft_strlen(NAME_CMD_STRING)) &&
			a->champ_header.prog_name[0] == '\0')
		g_is_name_first = 1;
	else if (ft_strnstr((*line) + g_index, COMMENT_CMD_STRING,
			ft_strlen(COMMENT_CMD_STRING)) &&
			a->champ_header.comment[0] == '\0')
		g_is_name_first = 0;
	else
		g_error = 1;
}

static void		check_name_or_comment(t_asm *a, char **line,
		size_t len)
{
	g_error = false;
	g_index = 0;
	while (g_index < len && ft_isblank((*line)[g_index]))
		g_index++;
	if (g_index < len && (*line)[g_index] == '.')
		check_signature(a, line);
	else
		g_error = 1;
	if (g_error)
	{
		ft_strdel(line);
		asm_exit(a, 1, "Error: incorrect .name or .comment.");
	}
	g_index += ft_strlen(g_is_name_first ? NAME_CMD_STRING :
			COMMENT_CMD_STRING);
}

static int		asm_take_name_or_comment_loop(t_asm *a, char **line,
	size_t len)
{
	const int length = (g_is_name_first ? PROG_NAME_LENGTH : COMMENT_LENGTH);

	while (g_index < len && (*line)[g_index] != '#' &&
		(*line)[g_index] != '\"' && (*line)[g_index] != ';'
		&& g_index2 < length)
	{
		if (g_is_name_first == 1)
			a->champ_header.prog_name
			[g_index2++] = (*line)[g_index++];
		else
			a->champ_header.comment
			[g_index2++] = (*line)[g_index++];
	}
	if ((*line)[g_index] == '\"')
	{
		ft_strdel(line);
		return (1);
	}
	ft_strdel(line);
	return (0);
}

static int		asm_take_name_or_comment(t_asm *a, char **line,
										size_t len)
{
	while (g_index < len && ft_isblank((*line)[g_index]))
		g_index++;
	if (((*line)[g_index]) != '\"')
	{
		ft_strdel(line);
		asm_exit(a, 1,
				"Error: champion name or comment must start with \".");
	}
	g_index++;
	if (asm_take_name_or_comment_loop(a, line, len))
		return (1);
	ft_strdel(line);
	while (get_next_line(a->fd, line) > 0)
	{
		g_index = 0;
		if (asm_take_name_or_comment_loop(a, line, ft_strlen(*line)))
			return (1);
		ft_strdel(line);
	}
	return (0);
}

void			asm_get_name_and_comment(t_asm *a)
{
	char	*line;
	size_t	len;

	g_index2 = 0;
	skip_whitespaces_and_comments(a, &line);
	if (line == NULL)
		asm_exit(a, 1, "Error: bad champion name.");
	len = ft_strlen(line);
	check_name_or_comment(a, &line, len);
	if (!asm_take_name_or_comment(a, &line, len))
		asm_exit(a, 1, "Error: bad champion name or comment.");
	g_index = 0;
	g_index2 = 0;
	g_is_name_first = g_is_name_first ? 0 : 1;
	skip_whitespaces_and_comments(a, &line);
	len = ft_strlen(line);
	check_name_or_comment(a, &line, len);
	if (!asm_take_name_or_comment(a, &line, len))
		asm_exit(a, 1, "Error: bad champion name or comment.");
	skip_whitespaces_and_comments(a, &line);
	a->line = line;
	asm_parse_insts(a);
}
