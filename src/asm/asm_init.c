/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_init.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 19:00:24 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 19:00:25 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

t_asm			*asm_init(void)
{
	t_asm *new_asm;

	new_asm = (t_asm *)malloc(sizeof(t_asm));
	new_asm->fd = -1;
	new_asm->line_number = 0;
	new_asm->i_of_code = 0;
	new_asm->champ_header.magic = COREWAR_EXEC_MAGIC;
	ft_bzero(new_asm->champ_header.prog_name, PROG_NAME_LENGTH + 1);
	ft_bzero(new_asm->champ_header.comment, COMMENT_LENGTH + 1);
	new_asm->file_name_cor = NULL;
	new_asm->instructions = NULL;
	return (new_asm);
}

t_instruction	*init_instruction(t_asm *asm_instance)
{
	t_instruction *new;
	t_instruction *tmp;

	tmp = get_last_instruction(asm_instance);
	new = (t_instruction *)malloc(sizeof(t_instruction));
	new->name = NULL;
	new->labels = NULL;
	new->arg1 = NULL;
	new->arg2 = NULL;
	new->arg3 = NULL;
	if (tmp)
		new->hex_id_for_write = tmp->hex_id_for_write + 1 +
			((tmp->codage) ? 1 : 0) +
			((tmp->arg1) ? tmp->arg1->len : 0) +
			((tmp->arg2) ? tmp->arg2->len : 0) +
			((tmp->arg3) ? tmp->arg3->len : 0);
	else
		new->hex_id_for_write = 0;
	return (new);
}

t_label			*label_init(char *label_name)
{
	t_label *label;

	label = (t_label *)malloc(sizeof(t_label));
	label->name = ft_strdup(label_name);
	return (label);
}

t_arg			*arg_init(char code, char label_flag,
						int value, char *label_name)
{
	t_arg	*new;

	new = (t_arg *)malloc(sizeof(t_arg));
	new->code = code;
	if (code == REG_CODE)
	{
		new->type = T_REG;
		new->len = 1;
	}
	else if (code == DIR_CODE)
		new->type = T_DIR;
	else
	{
		new->len = 2;
		new->type = T_IND;
	}
	new->label_flag = label_flag;
	new->value = value;
	new->label_name = label_name;
	return (new);
}
