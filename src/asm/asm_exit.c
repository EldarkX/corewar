/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_exit.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 19:04:28 by aantropo          #+#    #+#             */
/*   Updated: 2019/10/28 19:04:32 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/asm.h"

static void		free_arg(t_arg *arg)
{
	if (arg->label_name)
		ft_strdel(&arg->label_name);
	free(arg);
	arg = NULL;
}

static void		free_label(t_list **head)
{
	t_list	*labels;
	t_list	*tmp;
	t_label	*label;

	labels = *head;
	while (labels)
	{
		label = labels->content;
		if (label->name)
			ft_strdel(&label->name);
		free(labels->content);
		tmp = labels->next;
		free(labels);
		labels = NULL;
		labels = tmp;
	}
	*head = NULL;
}

void			asm_free_insts(t_asm *a)
{
	t_list			*insts;
	t_list			*tmp;

	insts = a->instructions;
	while (insts)
	{
		if (((t_instruction *)insts->content)->arg1)
			free_arg(((t_instruction *)insts->content)->arg1);
		if (((t_instruction *)insts->content)->arg2)
			free_arg(((t_instruction *)insts->content)->arg2);
		if (((t_instruction *)insts->content)->arg3)
			free_arg(((t_instruction *)insts->content)->arg3);
		free_label(&((t_instruction *)insts->content)->labels);
		if (((t_instruction *)insts->content)->name)
			ft_strdel(&((t_instruction *)insts->content)->name);
		free(insts->content);
		tmp = insts->next;
		free(insts);
		insts = tmp;
	}
}

void			asm_free_memory(t_asm *a)
{
	if (a)
	{
		if (a->fd > 0)
			close(a->fd);
		if (a->file_name_cor)
			ft_strdel(&a->file_name_cor);
		if (a->line)
			ft_strdel(&a->line);
		asm_free_insts(a);
		a->instructions = NULL;
		free(a);
		a = NULL;
	}
}

void			asm_exit(t_asm *a, int is_with_error, char *error_msg)
{
	if (is_with_error)
		ft_printf("%s\n", error_msg);
	asm_free_memory(a);
	exit(is_with_error ? -1 : 0);
}
